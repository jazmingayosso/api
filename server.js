console.log("Aqui funcionando con Nodemon");
var movimientosJSON= require('./movimientosv2.json')
var lstUsuarios= require('./usuarios.json')
var express=require('express');
var bodyparser = require('body-parser')
var requestJson= require('request-json')

var app=express();
app.use(bodyparser.json())
app.get('/',function(req,res)
{
  res.send('Hola API')
})
app.get('/v1/movimientos',function(req,res){
  res.sendfile('movimientosv1.json')
})
app.get('/v2/movimientos',function(req,res){
  res.send(movimientosJSON)
})
app.get('/v2/movimientos/:id',function(req,res)
{
  //console.log(req)

  console.log(req.params.id)
  //console.log(movimientosJSON);
  //res.send("Hemos recibido su petición de consulta del movimiento No:" + req.params.id)
  res.send(movimientosJSON[req.params.id-1])
})
//Alta de nuevo movimientos
app.post('/v2/movimientos',function(req,res){
 //console.log(req)
 //console.log(req.headers['authorization'])
 //if(req.headers['authorization']!=undefined)
 //{
var nuevo = req.body
nuevo.id=movimientosJSON.lengh +1
movimientosJSON.push(nuevo)

  res.send("Movimiento dado de alta")
//}
//else {
  //{
  //  res.send("No autorizado")
  //}
//}
}
)
//Modificacion
app.put('/v2/movimientos/:id',function(req,res){
var cambios = req.body
var actual= movimientosJSON[req.params.id-1]

if(cambios.importe!=undefined)
{
  actual.Importe=cambios.Importe
}
if(cambios.Ciudad!=undefined)
{
  actual.Ciudad = cambios.Ciudad
}

  res.send("Se ha modificado")
})
//Eliminaciòn
app.delete('/v2/movimientos/:id',function(req,res){
  var actual= movimientosJSON[req.params.id-1]
  movimientosJSON.push({
    "id": movimientosJSON.length+1,
    "Ciudad": actual.Ciudad,
    "Importe":actual.Importe *(-1),
    "concepto":"Negativo del "+req.params.id
  })
  res.send("Movimiento Anulado")
})
//QueryString
app.get('/v2/movimientosq',function(req,res){
  console.log(req.query);
  console.send("Recibido")

})
//Parametros
app.get('/v2/movimientosp/id:nombre/',function(req,res){
  console.log(req.params);
  console.send("Recibido")
  })
//------------------------------------------------------------
//----------USUARIOS
//Obtiene un usuario por id
app.get('/v1/usuarios/:id',function(req,res)
{
  console.log(req.params.id)
  res.send(lstUsuarios[req.params.id-1])
})
//LogIN
app.post('/v1/usuarios/login',function(req,res){
 var email = req.headers['email']
 var pass = req.headers['pass']
 var usuarioModificado=null
 for(i=0; i<lstUsuarios.length;i++)
 {
   if(lstUsuarios[i].password==pass && lstUsuarios[i].email==email )
   {

     usuarioModificado=lstUsuarios[i]
   }
 }

  if(usuarioModificado!=undefined){
    if(usuarioModificado.status==true){
      res.send("Error: El usuario se encuentra logueado.")
    }
    else{
    usuarioModificado.status=true
    lstUsuarios.push(usuarioModificado)
    res.send("Ha iniciado sesion")
    }
  }
  else {
    res.send("No se ha encontrado el usuario")
  }
})
//LogOUT
app.post('/v1/usuarios/logout/:id',function(req,res){
 var id= req.params.id
 var objUsuario= lstUsuarios[id-1]

   if(objUsuario!=undefined){
if(objUsuario.status==false){
  res.send("Error: El usuario no se encuentra logueado")
}
else {
    objUsuario.status=false
    lstUsuarios.push(objUsuario)
    res.send("Ha cerrado sesion")
  }
}
  else {
    res.send("No se ha encontrado el usuario")
  }
})

//Version 3 de la api conectada a MLAB
var urlMlabRaiz="https://api.mlab.com/api/1/databases/techumx/collections"
var apiKey= "apiKey=Sbc7s6geYXEHozouZ5H152rmNEvH32GG"
var clienteMlab=requestJson.createClient(urlMlabRaiz+'?'+apiKey);

app.get('/v3',function(req,res){
clienteMlab.get('',function(err, resM, body){
  var coleccionesUsuario=[]
  if(!err){
    for (var i = 0; i < body.length; i++) {
      if(body[i] !="system.indexes"){
      coleccionesUsuario.push({"recurso":body[i],"url":"/v3/"+body[i]})}
    }
  res.send(coleccionesUsuario)}
  else {
    res.send(err)
  }
})
})
//obtener usuarios
app.get('/v3/usuarios', function(req,res){
clienteMlab=requestJson.createClient(urlMlabRaiz+'/usuarios?'+apiKey);
 clienteMlab.get('',function(err, resM, body){
   res.send(body)
 })
})
//crear un usuarios
app.post('/v3/usuarios',function(req,res){
  clienteMlab=requestJson.createClient(urlMlabRaiz+'/usuarios?'+apiKey);
  clienteMlab.post('',req.body, function(err, resM,body){
    res.send(body)

  })
})
//obtener un usuario

app.get('/v3/usuarios/:id',function(req,res){
  clienteMlab=requestJson.createClient(urlMlabRaiz + '/usuarios')
  clienteMlab.get('?q={"id":'+req.params.id+'}&'+apiKey,
  function(err, resM, body){
    res.send(body)
  })
})
//modificar un usuario
app.put('/v3/usuarios/:id',function(req,res){
  var query= '?q={"id":'+req.params.id+'}&'
  clienteMlab=requestJson.createClient(urlMlabRaiz + '/usuarios'+ query)
  var cambios = '{"$set":'+JSON.stringify(req.body)  +'}'
  clienteMlab.put(query+apiKey,JSON.parse(cambios),function(err, resM, body){
    res.send("Registro Modificado")
  })
})
//v3 obtener movimientos
app.get('/v3/movimientos', function(req,res){
clienteMlab=requestJson.createClient(urlMlabRaiz+'/movimientos?'+apiKey);
 clienteMlab.get('',function(err, resM, body){
   res.send(body)
 })
})
//obtener un movimientos
app.get('/v3/movimientos/:id',function(req,res){
  clienteMlab=requestJson.createClient(urlMlabRaiz + '/movimientos')
  clienteMlab.get('?q={"idcuenta":'+req.params.id+'}&'+apiKey,
  function(err, resM, body){
    res.send(body)
  })
})
app.get('/v4/',function(req,res){

    res.sendfile('MejorasVarias.pdf')

})

app.listen(3000);
console.log("Escuchando en el puerto 3000");
