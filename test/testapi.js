var mocha= require('mocha')
var chai = require('chai')
var chaiHttp= require('chai-http')
var should= chai.should()
var server= require('../server')
//configurar chai con el modulo http
chai.use(chaiHttp)
//verificar que hay conecciòn a internet

describe('Test de conectividad',() => {
  it('Glogle funciona',(done) =>{
    chai.request('http://www.google.com.mx')
    .get('/')
    .end((err,res)=>{
      //console.log(res)
      res.should.have.status(200)
      done()
    })
  })
})
//
describe('Test de API usuarios',() => {
  it('Raiz de la api contesta',(done) =>{
    chai.request('http://localhost:3000')
    .get('/v3')
    .end((err,res)=>{
      //console.log(res)
      res.should.have.status(200)
      done()
    })
  })
})
describe('Test de API usuarios',() => {
  it('Raiz de la api funciona',(done) =>{
    chai.request('http://localhost:3000')
    .get('/v3')
    .end((err,res)=>{
      //console.log(res)
      res.should.have.status(200)
      res.body.should.be.a('array')
      done()
    })
  })
})

  it('Raiz de la api devuelve 2 collections',(done) =>{
    chai.request('http://localhost:3000')
    .get('/v3')
    .end((err,res)=>{
      //console.log(res)
      res.should.have.status(200)
      res.body.should.be.a('array')
      res.body.length.should.be.eql(2)
      done()
    })
  })

it('Raiz de la api devuelve los objetos correctos',(done) =>{
  chai.request('http://localhost:3000')
  .get('/v3')
  .end((err,res)=>{
    //console.log(res)
    res.should.have.status(200)
    res.body.should.be.a('array')
    res.body.length.should.be.eql(2)
    for (var i = 0; i < res.body.length; i++) {
      res.body[i].should.have.property('recurso')
      res.body[i].should.have.property('url')
    }
    done()
  })
})

describe('Test de API movimientos',() => {
  it('Raiz de la api movimientos contesta',(done) =>{
    chai.request('http://localhost:3000')
    .get('/v3/movimientos')
    .end((err,res)=>{
      //console.log(res)
      res.should.have.status(200)
      res.body.should.be.a('array')
      done()
    })
  })
})
describe('Test de API movimientos',() => {
  it('Raiz de la api movimientos por id contesta',(done) =>{
    chai.request('http://localhost:3000')

    .get('/v3/movimientos/"FI46 6178 9020 3633 63"')
    .end((err,res)=>{
      //console.log(res)
      res.should.have.status(200)
      res.body.should.be.a('array')
      res.body[0].should.have.property('idcuenta').which.is.eql("FI46 6178 9020 3633 63")
      done()
    })
  })
})
